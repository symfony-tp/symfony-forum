<?php

namespace App\Form;

use App\Entity\UserAuth;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('lastname')
            ->add('firstname')
            ->add('roles', ChoiceType::class, [
              'label' => 'Roles',
              'expanded' => true,
              'multiple' => true,
              'required' => false,
              'choices' => [
                  'ROLE_ADMIN' => "ROLE_ADMIN",
                  'ROLE_USER' => "ROLE_USER",
                  'ROLE_INSIDER' => "ROLE_INSIDER",
                  'ROLE_COLLABORATOR' => "ROLE_COLLABORATOR",
                  'ROLE_EXTERNAL' => "ROLE_EXTERNAL",
              ],
              'required' => false
            ])
            ->add('extra', ChoiceType::class, [
              'label' => 'Bannir l\'utilisateur',
              'expanded' => true,
              'multiple' => false,
              'required' => false,
              'mapped' => false,
              'choices' => [
                  'DEBAN' => "ROLE_USER",
                  'BAN' => "",
              ],
              'data' => 'ROLE_USER',
              'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAuth::class,
        ]);
    }
}
