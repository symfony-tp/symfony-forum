<?php

namespace App\Form;

use App\Entity\UserAuth;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfile extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
              'label' => 'Pseudo : '
            ])
            // ->add('email')
            ->add('lastname', TextType::class, [
              'label' => 'Nom : '
            ])
            ->add('firstname', TextType::class, [
              'label' => 'Prénom : '
            ])
            ->add('password', PasswordType::class, [
              'label' => 'Mot de passe : ',
              'data' => '',
              'required' => false,
              'empty_data' => '',
            ])
            ->add('password_confirm', PasswordType::class, [
              'label' => 'Confirmer le mot de passe : ',
              'data' => '',
              'mapped' => false,
              'required' => false,
              'empty_data' => '',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAuth::class,
        ]);
    }
}
