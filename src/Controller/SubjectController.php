<?php

namespace App\Controller;

use App\Entity\Subject;
use App\Entity\Message;
use App\Form\MessageType;
use App\Form\SubjectType;
use App\Repository\SubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Config\Definition\Exception\Exception;

#[Route('/subject')]
class SubjectController extends AbstractController
{
    #[Route('/', name: 'app_subject_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $subjects = $entityManager
            ->getRepository(Subject::class)
            ->findAll();

        return $this->render('subject/index.html.twig', [
            'subjects' => $subjects,
        ]);
    }
    
    // #[Route('/{id}/count', name: 'app_board_subject_count', methods: ['GET'])]
    // public function subjectCountByBoard(EntityManagerInterface $entityManager, int $id)
    // {  
    //     $subjectCount = $entityManager->getRepository(Subject::class);
    //     $countRow = $subjectCount->createQueryBuilder('a')
    //         ->select('count(a.id)')
    //         ->where('a.board = '. $id)
    //         ->getQuery()
    //         ->getSingleScalarResult();
        
    //     return new Response($countRow);
    // }

    #[Route('/new', name: 'app_subject_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $subject = new Subject();
        $form = $this->createForm(SubjectType::class, $subject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          if($this->IsGranted('ROLE_USER')) {
            // Get current user
            $user = $this->getUser();
            $subject->setUser($user);
            if ($subject->getCreatedAt() === null) {
              // $subject->setCreatedAt(\DateTime::createFromFormat('Y-m-d h:i:s', date("Y-m-d h:i:s")));
              $currentDate = new \DateTime('now');
              $subject->setCreatedAt($currentDate->format('Y-m-d H:i:s'));
            }
            $entityManager->persist($subject);
            $entityManager->flush();

            return $this->redirectToRoute('app_subject_show', ["id" => $subject->getId()], Response::HTTP_SEE_OTHER);
          }else {
            throw new Exception('Access Denied.');
          }
        }

        return $this->renderForm('subject/new.html.twig', [
            'subject' => $subject,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_subject_show', methods: ['GET', 'POST'])]
    public function show(Subject $subject): Response
    {

        return $this->render('subject/show.html.twig', [
            'subject' => $subject,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_subject_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Subject $subject, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(SubjectType::class, $subject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_subject_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('subject/edit.html.twig', [
            'subject' => $subject,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: 'app_subject_delete', methods: ['POST'])]
    public function delete(Request $request, Subject $subject, EntityManagerInterface $entityManager): Response
    {
      if($this->IsGranted('ROLE_ADMIN')) {
        if ($this->isCsrfTokenValid('delete'.$subject->getId(), $request->request->get('_token'))) {
          $entityManager->remove($subject);
          $entityManager->flush();
        }
      }else{
        throw new Exception('Access Denied.');
      }

        return $this->redirectToRoute('app_subject_index', [], Response::HTTP_SEE_OTHER);
    }
}
