<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Subject;
use App\Entity\Files;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/message')]
class MessageController extends AbstractController
{
    #[Route('/', name: 'app_message_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $messages = $entityManager
            ->getRepository(Message::class)
            ->findAll();
        $uploadsDir = "/uploads/";
        
        return $this->render('message/index.html.twig', [
            'messages' => $messages,
            'uploadsDir' => "http://" . $_SERVER['HTTP_HOST'] . $uploadsDir,
        ]);
    }

    #[Route('/{id}/new', name: 'app_message_new', methods: ['GET', 'POST'])]
    public function new(Subject $subjectId, Request $request, EntityManagerInterface $entityManager): Response
    {
        $message = new Message();

        if ($request->request->get('content') != null) {
          if($this->IsGranted('ROLE_USER')) {
            // Get current user
            $user = $this->getUser();
            $message->setUser($user);
            $message->setSubject($subjectId);
            $content = $request->request->get('content');
            $message->setContent($content);
            // get files inside request
            if ($message->getCreatedAt() === null) {
              $currentDate = new \DateTime('now');
              $message->setCreatedAt($currentDate->format('Y-m-d H:i:s'));
            }
            if($request->files->get('files') != null) {
              $filesRequest = $request->files->get('files');
              $uploadsDir = $this->getParameter('app.uploads_dir');
              // set uploads dir
              // Set the title of the file
              foreach($filesRequest as $key => $file) {
                $fileName = time() . "." . $file->guessExtension();
                $files = new Files();
                // Set the title of the file
                $files->setName($fileName);
                // Need to persist files before continue
                $entityManager->persist($files);
                // Add the file to message
                $message->addFile($files);
                $file->move($uploadsDir, $fileName);
              }
            }
          }else {
            throw new Exception('Access Denied.');
          }
          $entityManager->persist($message);
          $entityManager->flush();
          return $this->redirectToRoute('app_subject_show', ['id' => $subjectId->getId(), 'message' => $message->getId()], Response::HTTP_SEE_OTHER);
        }
        return $this->redirectToRoute('app_subject_show', ['id' => $subjectId->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}', name: 'app_message_show', methods: ['GET'])]
    public function show(Message $message): Response
    {
        return $this->render('message/show.html.twig', [
            'message' => $message,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_message_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Message $message, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_message_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('message/edit.html.twig', [
            'message' => $message,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: 'app_message_delete', methods: ['POST'])]
    public function delete(Request $request, Message $message, EntityManagerInterface $entityManager): Response
    {
      if($this->IsGranted('ROLE_ADMIN')) {
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {
            $entityManager->remove($message);
            $entityManager->flush();
        }
      }else {
          throw new Exception('Access Denied.');
        }

        return $this->redirectToRoute('app_message_index', [], Response::HTTP_SEE_OTHER);
    }
}
