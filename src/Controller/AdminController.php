<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
      $adminRoutes = [
        "/user/",
        "/message/",
        "/board/",
        "/category/",
        "/subject/"
      ];

      return $this->render('admin/index.html.twig', [
          'adminRoutes' => $adminRoutes,
      ]);
    }
}
