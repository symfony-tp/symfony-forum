<?php

namespace App\Controller;

use App\Entity\Files;
use App\Form\FilesType;
use App\Repository\FilesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Config\Definition\Exception\Exception;

#[Route('/files')]
class FilesController extends AbstractController
{
    #[Route('/', name: 'app_files_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        if($this->IsGranted('ROLE_ADMIN')) {
          $files = $entityManager
              ->getRepository(Files::class)
              ->findAll();

              return $this->render('files/index.html.twig', [
                'files' => $files,
              ]);
        }else {
          throw new Exception('Access Denied.');
        }
    }

    #[Route('/new', name: 'app_files_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
      throw new Exception('No route found for "GET http://127.0.0.1:8000/files/new"');
        // $file = new Files();
        // $form = $this->createForm(FilesType::class, $file);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $entityManager->persist($file);
        //     $entityManager->flush();

        //     return $this->redirectToRoute('app_files_index', [], Response::HTTP_SEE_OTHER);
        // }

        // return $this->renderForm('files/new.html.twig', [
        //     'file' => $file,
        //     'form' => $form,
        // ]);
    }

    #[Route('/{id}', name: 'app_files_show', methods: ['GET'])]
    public function show(Files $file): Response
    {
        if($this->IsGranted('ROLE_ADMIN')) {
          return $this->render('files/show.html.twig', [
              'file' => $file,
          ]);
        }else {
          throw new Exception('Access Denied.');
        }
    }

    #[Route('/{id}/edit', name: 'app_files_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Files $file, EntityManagerInterface $entityManager): Response
    {
        if($this->IsGranted('ROLE_ADMIN')) {
          $form = $this->createForm(FilesType::class, $file);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
              $entityManager->flush();

              return $this->redirectToRoute('app_files_index', [], Response::HTTP_SEE_OTHER);
          }

          return $this->renderForm('files/edit.html.twig', [
              'file' => $file,
              'form' => $form,
          ]);
        }else {
          throw new Exception('Access Denied.');
        }
    }

    #[Route('/delete/{id}', name: 'app_files_delete', methods: ['POST'])]
    public function delete(Request $request, Files $file, EntityManagerInterface $entityManager): Response
    {
      if($this->IsGranted('ROLE_ADMIN')) {
        if ($this->isCsrfTokenValid('delete'.$file->getId(), $request->request->get('_token'))) {
            $entityManager->remove($file);
            $entityManager->flush();
        }
      }else {
        throw new Exception('Access Denied.');
      }

        return $this->redirectToRoute('app_files_index', [], Response::HTTP_SEE_OTHER);
    }
}
