<?php

namespace App\Controller;

use App\Entity\UserAuth;
use App\Form\UserType;
use App\Form\UserProfile;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

#[Route('/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'app_user_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        if($this->IsGranted('ROLE_ADMIN')) {
          $users = $entityManager
              ->getRepository(UserAuth::class)
              ->findAll();

        }else {
          throw new Exception('Access Denied.');
        }

        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route('/new', name: 'app_user_new', methods: ['GET'])]
    public function new(): Response
    {
      throw new Exception('No route found for "GET http://127.0.0.1:8000/user/new"');
    }

    #[Route('/{id}', name: 'app_user_show', methods: ['GET'])]
    public function show(UserAuth $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserAuth $user, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // TODO 
            foreach ($request->request as $data){
              if($data['extra'] != "ROLE_USER"){
                $user->setRoles([]);
              } else {
                $user->setRoles(["ROLE_USER"]);
              }
            };
            $entityManager->flush();

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }    
    
    #[Route('/profile/{id}', name: 'app_user_profile', methods: ['GET', 'POST'])]
    public function profile(Request $request, UserAuth $user, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher): Response
    {
      $currentUser = $this->getUser();
      // check if the the user found by id is the same as current user
      if($user === $currentUser){
        
          $getCurrentPassword = $user->getPassword();
          $form = $this->createForm(UserProfile::class, $user);
          $form->handleRequest($request);
          $infoSystem = null;

          if ($form->isSubmitted() && $form->isValid()) {
            $getRequest = $request->request->all();
            if(!empty($getRequest['user_profile']['password']) && !empty($getRequest['user_profile']['password_confirm'])){
              if($getRequest['user_profile']['password'] === $getRequest['user_profile']['password_confirm']) {
                $user->setPassword(
                  $userPasswordHasher->hashPassword(
                          $user,
                          $form->get('password')->getData()
                      )
                  );
                  $infoSystem = "Le mot de passe a bien été changé.";
              } else {
                $infoSystem = "Les mots de passe ne correspondent pas.";
              }
            } else {
              $currentUser->setPassword($getCurrentPassword);
            }
            $entityManager->flush();
            // return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
          }

          return $this->renderForm('user/profile.html.twig', [
              'user' => $user,
              'form' => $form,
              'infoSystem' => $infoSystem
          ]);
      }else {
        throw new Exception('Access Denied.');
      }
    }

    #[Route('/delete/{id}', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, UserAuth $user, EntityManagerInterface $entityManager): Response
    {
      if($this->IsGranted('ROLE_ADMIN')) {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
          // if user is deleted, it will set all to null but he's not going to delete relations
          $messages = $user->getMessages();
          $boards = $user->getBoards();
          $categories = $user->getCategories();
          $subjects = $user->getSubjects();
          if($messages) {
            foreach($messages as $message){
              $message->setUser(null);
              $user->removeMessage($message);
            }   
          } 
          if($boards) {
            foreach($boards as $board){
              $board->setUser(null);
              $user->removeBoard($board);
            }            
          }     
          if($categories)   {
            foreach($categories as $category){
              $category->setUser(null);
              $user->removeCategory($category);
            }            
          }
          if($subjects) {
            foreach($subjects as $subject){
              $subject->setUser(null);
              $user->removeSubject($subject);
            }
          }
          $entityManager->remove($user);
          $entityManager->flush();
        }
      }else {
        throw new Exception('Access Denied.');
      }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
