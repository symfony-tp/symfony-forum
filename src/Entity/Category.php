<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'text')]
    private $created_at;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Board::class, cascade: ['persist', 'remove'])]
    private $board;

    #[ORM\ManyToOne(targetEntity: UserAuth::class, inversedBy: 'categories')]
    private $user;

    #[ORM\Column(type: 'array')]
    private $roles = [];

    public function __construct()
    {
        $this->board = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection<int, Board>
     */
    public function getBoard(): Collection
    {
        return $this->board;
    }

    public function addBoard(Board $board): self
    {
        if (!$this->board->contains($board)) {
            $this->board[] = $board;
            $board->setCategory($this);
        }

        return $this;
    }

    public function removeBoard(Board $board): self
    {
        if ($this->board->removeElement($board)) {
            // set the owning side to null (unless already changed)
            if ($board->getCategory() === $this) {
                $board->setCategory(null);
            }
        }

        return $this;
    }

    public function getUser(): ?UserAuth
    {
        return $this->user;
    }

    public function setUser(?UserAuth $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
