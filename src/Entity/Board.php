<?php

namespace App\Entity;

use App\Repository\BoardRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BoardRepository::class)]
class Board
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'board')]
    private $category;

    #[ORM\ManyToOne(targetEntity: UserAuth::class, inversedBy: 'boards')]
    private $user;

    #[ORM\OneToMany(mappedBy: 'board', targetEntity: Subject::class, cascade: ['persist', 'remove'])]
    private $subject;

    #[ORM\Column(type: 'string', length: 150, nullable: true)]
    private $description;

    public function __construct()
    {
        $this->subject = new ArrayCollection();
    }   
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): ?UserAuth
    {
        return $this->user;
    }

    public function setUser(?UserAuth $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Subject>
     */
    public function getSubject(): Collection
    {
        return $this->subject;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subject->contains($subject)) {
            $this->subject[] = $subject;
            $subject->setBoard($this);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subject->removeElement($subject)) {
            // set the owning side to null (unless already changed)
            if ($subject->getBoard() === $this) {
                $subject->setBoard(null);
            }
        }

        return $this;
    }
    
    /**
     * Return the last subject
     */
    public function lastSubject()
    {
      $lastSubject = null;
      if (!$this->subject->isEmpty())
      {
        $getLastSubject = $this->subject->last(); 
        $lastSubject = array(
          'id' => $getLastSubject->getId(),
          'name' => $getLastSubject->getName(),
          'user' => $getLastSubject->getUser(),
          'createAt' => $getLastSubject->getCreatedAt(),
        );
      }
      return $lastSubject; 
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
