<?php

namespace App\Entity;

use App\Repository\SubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SubjectRepository::class)]
class Subject
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'text')]
    private $created_at;

    #[ORM\Column(type: 'text')]
    private $content;

    #[ORM\ManyToOne(targetEntity: UserAuth::class, inversedBy: 'subjects')]
    private $user;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: Message::class, cascade: ['persist', 'remove'])]
    private $messages;

    #[ORM\ManyToOne(targetEntity: Board::class, inversedBy: 'subject')]
    private $board;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUser(): ?UserAuth
    {
        return $this->user;
    }

    public function setUser(?UserAuth $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setSubject($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSubject() === $this) {
                $message->setSubject(null);
            }
        }

        return $this;
    }

        /**
     * Return the last message
     */
    public function lastMessage()
    {
      $lastSubject = null;
      if (!$this->messages->isEmpty())
      {
        $getLastSubject = $this->messages->last(); 
        $lastSubject = array(
          'id' => $getLastSubject->getId(),
          'name' => $getLastSubject->getContent(),
          'user' => $getLastSubject->getUser(),
          'createAt' => $getLastSubject->getCreatedAt(),
        );
      }
      return $lastSubject; 
    }

    public function getBoard(): ?Board
    {
        return $this->board;
    }

    public function setBoard(?Board $board): self
    {
        $this->board = $board;

        return $this;
    }
}
