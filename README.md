# Installation

## Create .env.local and setup the connexion at the database

### You can import "base_de_donnee_dump" in the root of the projet if you want data

```
DATABASE_URL="mysql://user:password@127.0.0.1:3306/bd_name?serverVersion=5.7&charset=utf8mb4"
```

## Make a migrate

```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:schema:validate
```

## Start the project

run :  
```
composer install  
yarn install  
yarn watch
```

```
symfony server:start
```
